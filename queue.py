queue = [["shipment1", 1], ["shipment2", 0], ["shipment3", 3], ["shipment4", 3], ["shipment5", 2]]

def show_list():
    for i in queue:
        print(i)

def add_shipment(shipment_nr, priority_nr):
    priority_nr = int(priority_nr)
    queue.append([f"shipment{shipment_nr}", priority_nr])
    priority()
    return f"Shipment{shipment_nr} with {priority_nr} priority has been added"

def priority():
    global x
    x = 0
    for i in reversed(range(4)):
        for j in queue:
            if i == j[1]:
                x = j
                break
        if x != 0:
            break

def remove_shipment():
    queue.remove(x)
    priority()

def check_first_in_queue():
    try:
        print(f"{x[0]} with {x[1]} priority is first in queue")
    except:
        print("List is empty")

def is_empty():
    if queue.__len__() == 0:
        print("Queue is empty")
    else:
        print(f"Queue has {queue.__len__()} shipments")
priority()
